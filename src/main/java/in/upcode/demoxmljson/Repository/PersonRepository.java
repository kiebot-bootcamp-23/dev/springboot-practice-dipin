package in.upcode.demoxmljson.Repository;

import in.upcode.demoxmljson.Model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
@Repository
public interface PersonRepository extends JpaRepository<Person,Integer> {

    Optional<Person> findByNameIgnoreCase(String name);

//    @Query(value="select * from person p where p.age=?1  limit 1", nativeQuery = true)
//    Optional<Person> findByAge(int age);

    @Query(" select p from Person p where p.age=?1")
    public Optional<Person> findByAge(int age);
    Optional<Person> findByPlace(String place);
}
