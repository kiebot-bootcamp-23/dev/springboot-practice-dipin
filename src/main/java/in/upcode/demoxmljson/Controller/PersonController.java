package in.upcode.demoxmljson.Controller;

import in.upcode.demoxmljson.Model.Person;
import in.upcode.demoxmljson.Repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/person")
public class PersonController {
@Autowired
    PersonRepository personRepository;
    List<Person> listOfPeople = List.of(
            new Person(1,"Dipin",21,"Kannur", LocalDate.of(2000,05,06)),
            new Person(2,"Ranjul",22,"Kasargod",LocalDate.of(1999,12,25)),
            new Person(3,"Febin",23,"Kochi",LocalDate.of(2002,05,14))
            );
    @RequestMapping("")
     List<Person> showOnePerson(){

                return personRepository.findAll(Sort.by("name"));
    }

    @RequestMapping("/name/{name}")
    Person listPersonByName(@PathVariable("name") String name){
        final Optional<Person> optionalPerson= personRepository.findByNameIgnoreCase(name);

        if(optionalPerson.isPresent()){
            return optionalPerson.get();
        }
       throw new RuntimeException("Not found");
    }

    @RequestMapping("/place/{place}")
    Person listPersonByPlace(@PathVariable("place") String place){
        final Optional<Person> optionalPerson= personRepository.findByPlace(place);

        if(optionalPerson.isPresent()){
            return optionalPerson.get();
        }
        throw new RuntimeException("Not found");
    }

    @RequestMapping("/age/{age}")
    Person listPersonByAge(@PathVariable("age") Integer age){
        final Optional<Person> optionalPerson= personRepository.findByAge(age);

        if(optionalPerson.isPresent()){
            return optionalPerson.get();
        }
        throw new RuntimeException("Not found");
    }

    @RequestMapping("/{id}")
    Person listPersonById(@PathVariable("id") Integer id){
        final Optional<Person> optionalPerson= personRepository.findById(id);

        if(optionalPerson.isPresent()){
            return optionalPerson.get();
        }
        throw new RuntimeException("Not found");
    }
}
